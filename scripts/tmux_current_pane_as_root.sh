#!/usr/bin/env bash

path=$1
session_id=$2

tmux command-prompt -p "(Project Root)" -I "${path}" "attach-session -c '%1' -t ${session_id}; setenv PROJECT_ROOT '%1'"
